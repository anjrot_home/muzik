import React, { Component } from "react";
import axios from "axios";
import "../css/style.css";
import MenuCategory from "./MenuCategory";
import AlbumImage from "./AlbumImage";
import InfoTag from "./InfoTag";

class App extends Component {
  state = {
    data: null,
    info: [],
    infoTag: [],
    category: null
  };

  componentDidMount = () => {
    this.getData();
  };

  getData = () => {
    return axios
      .get("https://itunes.apple.com/us/rss/topalbums/limit=100/json")
      .then(response => {
        const data = response.data.feed.entry;
        this.setState({ data });
        let category = data.map(item => item.category.attributes.term);
        category = [...new Set(category)];
        this.setState({ category });
      })
      .catch(err => err);
  };

  handleClick = e => {
    e.preventDefault();
    console.log(e.target);

    const getCategoryName = this.state.data;
    let info = [];
    getCategoryName.find(cat => {
      if (cat.category.attributes.term === e.target.name) {
        info.push(cat);
      }
      return false;
    });
    this.setState({ info });
  };

  handleInfo = e => {
    e.preventDefault();
    console.log(e.target.name);
    const info = this.state.info;
    info.filter(infoTag => {
      if (e.target.name === infoTag.id.attributes["im:id"]) {
        this.setState({ infoTag });
      }
    });
  };

  render() {
    const info = this.state.info;
    const infoTag = this.state.infoTag;
    console.log(infoTag);

    return (
      <div className="container-fluid">
        <div className="row">
          <div className="col-md-3 col-xs-12 menu">
            <h3>MUZIK</h3>
            <div className="side-bar menu-side">
              <MenuCategory
                category={this.state.category}
                onClick={this.handleClick}
              />
            </div>
          </div>
          <div className="col-md-9 col-xs-12 main-container">
            <div className="search">
              <InfoTag data={infoTag} />
            </div>
            <div className="container albumImage">
              <AlbumImage info={info} onClick={this.handleInfo} />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
