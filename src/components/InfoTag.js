import React from "react";

const InfoTag = props => {
  return (
    <div>
      <div class="search-title">
        {!props.data["im:image"] ? (
          <h4>Music song</h4>
        ) : (
          <div class="row">
            <div class="col-md-3 info-img">
              <img src={props.data["im:image"][2].label} alt="" />
            </div>
            <div class="col-md-9 info-tag">
              <ul>
                <li>
                  <strong>Artist:</strong> {props.data["im:artist"].label}
                </li>
                <li>
                  <strong>Album:</strong> {props.data["im:name"].label}
                </li>
                <li>
                  <strong>Rights:</strong> {props.data.rights.label}
                </li>
              </ul>
            </div>
          </div>
        )}
      </div>
    </div>
  );
};

export default InfoTag;
