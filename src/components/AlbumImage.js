import React from "react";

const AlbumImage = props => {
  return (
    <div>
      <div className="row colum-content">
        {props.info.map((item, index) => (
          <div className="col-md-3 colums-5" key={index}>
            <a href="#" onClick={props.onClick}>
              <img
                src={item["im:image"][2].label}
                alt=""
                name={item.id.attributes["im:id"]}
              />
            </a>
            <p className="caption">{item["im:artist"].label}</p>
          </div>
        ))}
      </div>
    </div>
  );
};

export default AlbumImage;
