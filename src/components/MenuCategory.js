import React from "react";

const MenuCategory = props => {
  return (
    <ul>
      <h4>MUSIC</h4>
      {!props.category ? (
        <p>Loading...</p>
      ) : (
        props.category.map((it, index) => (
          <li key={index}>
            <a href="#" onClick={props.onClick} name={it}>
              {it}
            </a>
          </li>
        ))
      )}
    </ul>
  );
};

export default MenuCategory;
